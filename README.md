# release-the-leviathan

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Destruction_of_Leviathan.png/250px-Destruction_of_Leviathan.png)

## Docker image

[Link to file](Dockerfile)

``` sh
docker build -t p0l1cy:release-the-leviathan:latest .
```

## GitLab CI pipeline

[Link to file](.gitlab-ci.yml)

*WARNING*: The pipeline succeeds in building the image but fails in the scanning step, as the RPM package currently has an open vulnerability with High CVE severity.

## Kubernetes *StatefulSet*

[Link to file](statefulset.yaml)

Tested in a local environment using [`kind`](https://kind.sigs.k8s.io/) v0.9.0.

``` sh
kubectl create -f statefulset.yaml
```

## Text manipulation

Print each directory in `PATH` in a separate line. Also, replace any `/home/${USER}` by tilde (`~`).

### Bash

``` sh
env | grep "^PATH=" | tr ":" "\n" | sed 's/\/home\/'"$(whoami)"'/~/'
```

### Python

Tested with Python 3.8

``` python
from os import environ

for d in environ.get("PATH").split(":"):
    print(d.replace("/home/{}".format(environ.get("USER")), "~"))
```

### Terraform

See the [`iam-module`](iam-module) directory.
