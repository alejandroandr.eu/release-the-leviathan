resource "aws_iam_group" "group" {
  name = "group-${var.environment}"
}

resource "aws_iam_group_policy_attachment" "policy_attachment" {
  group      = aws_iam_group.group.name
  policy_arn = aws_iam_policy.policy.arn
}
