# iam-module

Module tested with [`localstack`](https://github.com/localstack/localstack). To test it:

``` sh
docker run --rm -p 4566:4566 -p 4571:4571 localstack/localstack
terraform init
terraform apply
```

In a real-life scenario, some outputs might be useful.
