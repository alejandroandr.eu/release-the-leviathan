FROM fedora@sha256:e2daaac9dc4de3363bf541908f7a4ad156df7e94794fc515992619eae3eb2344

ARG ARCH=x86_64
ARG LITECOIN_VERSION=0.18.1

RUN set -ex; \
    # Download and extract Litecoin
    curl -O https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-${ARCH}-linux-gnu.tar.gz; \
    curl -O https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc; \
    grep litecoin-${LITECOIN_VERSION}-${ARCH}-linux-gnu.tar.gz litecoin-${LITECOIN_VERSION}-linux-signatures.asc | sha256sum -c; \
    tar -xzf litecoin-${LITECOIN_VERSION}-${ARCH}-linux-gnu.tar.gz; \
    # Create a dedicated litecoin system group & system user
    groupadd --gid 54321 --system litecoin; \
    useradd --uid 54321 --system -g litecoin litecoin; \
    # Create litecoind datadir & give permissions to litecoin user
    mkdir /litecoin-${LITECOIN_VERSION}/data; \
    chown litecoin:litecoin /litecoin-${LITECOIN_VERSION}/data;

WORKDIR /litecoin-${LITECOIN_VERSION}
USER litecoin
ENTRYPOINT ["bin/litecoind", "-datadir=data"]
